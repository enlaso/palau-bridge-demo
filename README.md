# Client Example for [ENLASO](http://www.enlaso.com/)'s Palau Bridge #

This project is an example of a HTML+JavaScript client for Palau Bridge, a REST API that allows client applications to access Palau, [ENLASO](http://www.enlaso.com/)'s translation management system.

For more information, see:

* The main documentation: https://palau.enlaso.com/shared/api-docs/index.html
* The details for each end-points of the service: https://palau.enlaso.com/shared/api-docs/end-points.html

### License ###

This project is licensed under the **Apache License Version 2.0** (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

### Getting Started ###

* Copy the project's files in a directory.
* Open the main HTML file at the root.
* Make sure to enter the URL of the server you want to use in the *Palau API Host* field
* Obtain a session token with your credentials
* Use the token with the other Web service's function

Please note, for maximum compatibility with this project we suggest using **Google Chrome** as your web browser.

### Contact ###

For any question about how to integrate with [ENLASO](http://www.enlaso.com/)'s Portal or how to obtain credentials, please contact your [ENLASO](http://www.enlaso.com/) representative, or contact us online at http://www.enlaso.com/contact