/*===========================================================================
  Copyright (C) 2016-2017 by ENLASO Corporation
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
  http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
  implied. See the License for the specific language governing 
  permissions and limitations under the License.
 ===========================================================================*/
 
var palauAPIApp = angular.module('palauAPIApp', ['angularFileUpload']);

palauAPIApp.controller('PalauAPICtrl', ['$scope', '$http', '$window', '$upload', function ($scope, $http, $window, $upload) {

	// Config: general
	$scope.apiHost = "https://palau.enlaso.com";
	$scope.showFileLink = false;

	// Config: token/auth
	$scope.username;
	$scope.keyId;
	$scope.pwrd;
	$scope.authHeader;

	// Config: language pair check
	$scope.checkLangValue = "en-US_ZH";
	
	// Config: get file info
	$scope.getFileInfoFileID;
	
	// Config: get translated file
	$scope.getFileTranslationFileID;
	
	// Config: get file metrics
	$scope.getFileMetricsFileID;
	
	// Config: cancel file
	$scope.cancelFileFileID;
	
	// Config: accept file
	$scope.acceptFileFileID;
	
	// Config: trigger pseudo-translation
	$scope.triggerPseudoTransFileID;

	// Config: add project files info
	$scope.addProjFilesForm = {};
	$scope.addProjFilesForm.source = "en-US";
	$scope.addProjFilesForm.target;
	$scope.addProjFilesForm.configId;
	$scope.addProjFilesProjID;
	$scope.addProjFilesSelection; // selected files model

	// Config: mark pair as ready
	$scope.markPairAsReadyProjID;
	$scope.markPairAsReadyPair;

	// Config: list project files
	$scope.listProjFilesProjID;
	
	// Config: create new project
	$scope.createProjectForm = {};
	$scope.createProjectForm.source = "en-US";
	$scope.createProjectForm.targets = ["fr-FR", "de-DE"];
	$scope.createProjectForm.slug = "my_project_name";
	// Config: list project languages info
	$scope.listProjLangsProjID;
	
	// Config: delete project
	$scope.deleteProjProjID;
	
	// Config: create project
	$scope.createProjProjParameters = JSON.stringify(
		{
			"projDue": "string",
			"slug": "string",
			"source": "string",
			"targets": [ "string" ]
		}, null, 2);
	
	$scope.createProjIsTest;
	
	// Config: check task
	$scope.checkTaskTaskID;
	
	// Config: list available translations for project
	$scope.listProjTranslationsProjID;
	$scope.listProjTranslationsLangPair;
	
	// Config: set callback
	$scope.setCallBackProjD;
	$scope.setCallBackType = "transready";
	$scope.setCallBackURL;
	//$scope.setCallBackFileIdParameterType = "Body";
	$scope.setCallBackMethod;
	
	// Endpoint paths
	$scope.pathTokenBase = "/rest/v1/tokens";
	$scope.pathCheckLangPair = "/rest/v1/pairs";
	$scope.pathCheckTask = "/rest/v1/tasks";
	$scope.pathFileBase = "/rest/v1/files";
	$scope.pathProjectBase = "/rest/v1/projects";
	
	// Server responses
	$scope.resultGetToken;
	$scope.responseStatus;
	$scope.responseDetails;
	
	// Miscellaneous functions
	
	// Create preview of auth header as user enters auth info
	$scope.buildAuthHeader = function () {
		if ($scope.username && $scope.pwrd) {
			$scope.authHeader = "Basic "+window.btoa($scope.username+":"+$scope.pwrd);
		}
		else {
			$scope.authHeader = null;
		}
	}
	// Initialize Auth Header
	$scope.buildAuthHeader();
	
	// Clear response display during call
	$scope.clearOutput = function ( ) {
		$scope.showFileLink = false;
		$scope.responseStatus = 'Processing...';
		$scope.responseDetails = '';
	}

	
	// API request functions
	
	$scope.getToken = function ( ) {

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathTokenBase,
			method: 'POST',
			data: $scope.keyId,
			headers: {'Content-Type': 'text/plain', 'Authorization': $scope.authHeader}
		})
		.then(function(response) { // Success
			console.log(response);
			$scope.resultGetToken = response.data;
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = response.data;
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.deleteProject = function ( ) {
		
		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.deleteProjProjID,
			method: 'DELETE',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = null;
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	
	$scope.deleteToken = function ( ) {
		
		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathTokenBase+"/"+$scope.resultGetToken,
			method: 'DELETE',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.resultGetToken = null;
			$scope.responseDetails = null;
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.checkLangPair = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathCheckLangPair+"/"+$scope.checkLangValue,
			method: 'GET',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.listLangPairs = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathCheckLangPair,
			method: 'GET',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.getFileInfo = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.getFileInfoFileID,
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}

	$scope.getFileTranslation = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.getFileTranslationFileID+"/translation",
			method: 'GET',
			data: '',
			headers: { 'Content-Type': 'text/plain', 'token': $scope.resultGetToken }
		})
		.then(function(response) { // Success
			
			// Attempt to open/save this data as a zip file
			function saveContent(response) {
				
				var fileLink = document.createElement('a');
				var fileName = response.headers("Palau-Translated-Filename");
				fileLink.innerHTML = fileName;
				var linkHolder = angular.element('#linkHolder');
				linkHolder.empty();
				// Add link to the DOM (Seems Firefox also requires the link to be on the DOM?)
				linkHolder.append(fileLink);
				$scope.showFileLink = true;

				// Check if browser supports download attr on anchor (currently not supported on IE, Safari)
				if (typeof fileLink.download === 'string') {
					fileLink.download = fileName;
					fileLink.href = 'data:application/zip;base64,' + response.data;

					// Simulate a click to auto-download file
					fileLink.click();
				}
				else { // IE, etc
					fileLink.href = 'data:application/zip;base64,' + response.data+'/'+fileName;
				}
			}
			
			// Run the file save function
			saveContent(response);

			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.getFileMetrics = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.getFileMetricsFileID+"/metrics",
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.cancelFile = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.cancelFileFileID+"/cancel",
			method: 'PUT',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.createProject = function ( ) {
		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}
		$scope.clearOutput();
		$scope.createProjectForm.targets = $scope.createProjectForm.targets.toString().split(',');
		for (var i=0, len=$scope.createProjectForm.targets.length; i<len; i++) {
			$scope.createProjectForm.targets[i] = $scope.createProjectForm.targets[i].trim();
		}
		$http({
			url: $scope.apiHost+$scope.pathProjectBase,
			method: 'POST',
			headers: {'Content-Type': 'application/json', 'token': $scope.resultGetToken},
			data: $scope.createProjectForm,
		})
		.then(function(response) { // Success
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	$scope.addProjFiles = function ( ) {
		
		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$upload.upload({
			file: $scope.addProjFilesSelection,
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.addProjFilesProjID+"/files",
			method: 'POST',
			headers: {'Content-Type': 'multipart/form-data', 'token': $scope.resultGetToken},
			data: $scope.addProjFilesForm,
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.markPairAsReady = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.markPairAsReadyProjID+"/"+$scope.markPairAsReadyPair+"/ready",
			method: 'PUT',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.listProjFiles = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.listProjFilesProjID+"/files",
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.listProjects = function ( ) {
		
		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase,
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.listProjLangs = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.listProjLangsProjID+"/pairs",
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.listProjTranslations = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathProjectBase
				+"/"
				+$scope.listProjTranslationsProjID
				+"/"
				+$scope.listProjTranslationsLangPair
				+"/translations",
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.checkTask = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathCheckTask+'/'+$scope.checkTaskTaskID,
			method: 'GET',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.setCallback = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();
		
		$http({
			url: $scope.apiHost+$scope.pathProjectBase+"/"+$scope.setCallBackProjD+"/callbacks/"+$scope.setCallBackType
				+(( !$scope.setCallBackMethod || ($scope.setCallBackMethod=="")) ? "" : ("?method="+$scope.setCallBackMethod)),
			method: 'PUT',
			data: $scope.setCallBackURL,
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
	
	$scope.acceptFile = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.acceptFileFileID+"/accept",
			method: 'PUT',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}

	$scope.triggerPseudoTrans = function ( ) {

		// Token check
		if ( !$scope.resultGetToken ) {
			$window.alert("Please make sure you enter a valid token");
			return;
		}

		$scope.clearOutput();

		$http({
			url: $scope.apiHost+$scope.pathFileBase+"/"+$scope.triggerPseudoTransFileID+"/pseudotrans",
			method: 'PUT',
			data: '',
			headers: {'Content-Type': 'text/plain', 'token': $scope.resultGetToken}
		})
		.then(function(response) { // Success
			$scope.responseStatus = response.status + " - " + response.statusText;
			$scope.responseDetails = JSON.stringify(response.data, null, 1);
			console.log(response);
		},
		function(response) { // Error
			console.log(response);
			$scope.responseStatus = response.status + " - " + response.statusText;
			if ( response.data != null ) $scope.responseDetails = response.data.message;
		})
	}
}]);